<?php

require_once '\Settings.php';

require_once Settings::getRoot().'model\Comment.php';
require_once Settings::getRoot().'db\DBConnector.php';

class Comment_mapper {
    
    private static function create($line){
        
        $current = new Comment();
        $current->setId($line['idpss_grr_comment']);
        $current->setPhoto($line['id_photo']);
        $current->setUser($line['id_user']);
        $current->setComment($line['comment']);
        
        return $current;
    }
    
    //Select specific comment
    public static function select($id){
        
        $db = DBConnector::getConnection();
        
        try{
            
            $pstmt = $db->prepare("SELECT * FROM pss_grr_comment WHERE id =:id;");
            
            $pstmt->bindValue(':id', $id, PDO::PARAM_int);
            
            $pstmt->execute();
            
            $comment = $pstmt->fetchAll();
            
            
        } catch (Exception $ex) {
            echo "Couldn't find comments";

        }
        
        return $comment; 
        
    }
    
    //Select all comments for photo
    public static function selectComments($photoId){
        
        $db = DBConnector::getConnection();
        
        try{
            
            $pstmt = $db->prepare("SELECT * FROM pss_grr_comment WHERE id_photo =:photoid;");
            
            $pstmt->bindValue(':photoid', $photoId, PDO::PARAM_INT);
            $pstmt->execute();
            $pstmt = $pstmt->fetchAll();
            
            $commentArray = array();
            
            foreach ($pstmt as $line){
                $com = self::create($line);
                array_push($commentArray, $com);
            }
            
        } catch (Exception $ex) {
            echo "Couldn't find comments";

        }
        
        return $commentArray;
    }
    
    //Insert new comment for photo:
    public static function insert($photoId, $userId, $comment){
        
        $db = DBConnector::getConnection();
        
        try{
            //INSERT INTO `pss`.`pss_grr_comment` (`idpss_grr_comment`, `id_photo`, `id_user`, `comment`) 
            //VALUES (NULL, '7', '6', 'test from phpmyadmin');
            $pstmt = $db->prepare("INSERT INTO pss_grr_comment (id_photo, id_user, comment) "
                                . "VALUES (:photoid, :userid, :comment)");
            
            $pstmt->bindValue(':photoid', $photoId, PDO::PARAM_INT);
            $pstmt->bindValue(':userid', $userId, PDO::PARAM_INT);
            $pstmt->bindValue(':comment', $comment, PDO::PARAM_STR);
            
            $pstmt->execute();
            
        } catch (Exception $ex) {
            echo "Could not insert new comment";
        }
    }
}

?>
