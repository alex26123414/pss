<?php

require_once '\Settings.php';
require_once Settings::getRoot() . 'db\DBConnector.php';

class PhotoShare_mapper {

    private static function create($line) {

        // Create new object
        $current = new PhotoShare();
        $current->setId($line['idpss_grr_photo']);
        $current->setName($line['name']);
        $current->setUser($line['id_user']);
        $current->setContent($line['content']);

        // Return object
        return $current;
    }

    public static function share($email, $photoId) {
        // Get database connection
        $db = DBConnector::getConnection();

        // Create a new scenario
        try {
            $user = User_mapper::verifyUser($email);
            if ($user) {
                $userId = $user->getId();
                // Prepare SQL statement

                $pstmt = $db->prepare("SELECT idpss_grr_photo_share FROM pss_grr_photo_share WHERE id_user=:uid AND id_photo=:pid");

                // Bind SQL values
                $pstmt->bindValue(':uid', $userId, PDO::PARAM_INT);
                $pstmt->bindValue(':pid', $photoId, PDO::PARAM_INT);
                //var_dump($pstmt);
                // Execute SQL query
                $pstmt->execute();
                if (!($pstmt->fetch())) {
                    $pstmt = $db->prepare("INSERT INTO pss_grr_photo_share (id_user,id_photo) VALUES (:uid,:pid)");

                    // Bind SQL values

                    $pstmt->bindValue(':uid', $userId, PDO::PARAM_INT);
                    $pstmt->bindValue(':pid', $photoId, PDO::PARAM_INT);
                    //var_dump($pstmt);
                    // Execute SQL query
                    $pstmt->execute();

                    echo "<br>Email $email Shared<br>";
                } else {

                    echo "The user exists";
                }
            } else {

                echo "Email doesnt exist or wrong Email";
            }
        } catch (PDOException $e) {
            echo "Couldnt share";
        }

        return;
    }

}
