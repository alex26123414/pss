<?php

require_once '\Settings.php';
require_once Settings::getRoot().'db\DBConnector.php';
 
class Image_mapper {
    
        private static function create($line) {

        // Create new object
        $current = new Photo();
        $current->setId($line['idpss_grr_photo']);
        $current->setName($line['name']);
        $current->setUser($line['id_user']);
        $current->setContent($line['content']);

        // Return object
        return $current;
    }
    public static function select($id) {
        // Get database connection
        $db = DBConnector::getConnection();

        // Create a new scenario
        try {
            // Prepare SQL statement
            $pstmt = $db->prepare("SELECT idpss_grr_photo FROM pss_grr_photo WHERE id_user =:userid;");
            // Bind SQL values
            $pstmt->bindValue(':userid', $id, PDO::PARAM_INT);
            // Execute SQL query
            $pstmt->execute();
            // Fetch all results 
            $pstmt = $pstmt->fetchAll();           
            // Loop all results as lines
            $array=array();
            foreach ($pstmt as $line) {
                // Add current object to array
               // echo $line[0];
               //$image = self::create($line);
                //array_push($array, $image->getContent());
                array_push($array,$line[0]);
            }
           // var_dump($image);
        } catch (PDOException $e) {
            echo "Couldnt select id from photo";
        }
                try {
            // Prepare SQL statement
            $pstmt = $db->prepare("SELECT id_photo FROM pss_grr_photo_share WHERE id_user =:userid;");
            // Bind SQL values
            $pstmt->bindValue(':userid', $id, PDO::PARAM_INT);
            // Execute SQL query
            $pstmt->execute();           
            // Fetch all results  
            $pstmt = $pstmt->fetchAll();          
            // Loop all results as lines         
            foreach ($pstmt as $line) {
                // Add current object to array
               // echo $line[0];
               // $image = self::create($line);
                //array_push($array, $image->getContent());
                array_push($array,$line[0]);
            }
           // var_dump($image);
        } catch (PDOException $e) {
            echo "Couldnt select id from photo share";
        }
        return $array;
        //return $image;
    }
   
    public static function insert($name,$userId) {
        // Get database connection
        $db = DBConnector::getConnection();
        
        // Create a new scenario
        try {
            // Prepare SQL statement
            $pstmt = $db->prepare("INSERT INTO pss_grr_photo (name,id_user)VALUES (:name,:uid)");

            // Bind SQL values
           
            $pstmt->bindValue(':uid', $userId, PDO::PARAM_INT);
            $pstmt->bindValue(':name', $name, PDO::PARAM_STR);
            //var_dump($pstmt);
            // Execute SQL query
            $pstmt->execute();
             echo "<br>File $name uploaded<br>";
        } catch (PDOException $e) {
            echo "catch";
        }
         $last_id = $db->lastInsertId();
        //$query = "INSERT INTO pss_grr_photo (name, content,id_user)VALUES ($name,$content,$userId)";
        //mysql_query($query) or die('Error, query failed'); 
        return $last_id;
        
    }
    
}
