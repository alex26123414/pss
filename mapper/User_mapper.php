<?php

require_once '\Settings.php';

require_once Settings::getRoot() . 'db\DBConnector.php';

class User_mapper {

    //put your code here
    private static function create($line) {

        // Create new object
        $current = new User();
        $current->setId($line['idpss_grr_user']);
        $current->setEmail($line['email']);
        $current->setPass($line['pass']);
        $current->setSalt($line['salt']);

        // Return object
        return $current;
    }

    /**
     * Selects a user from the DB
     * @param String $email
     * @param String $pass (as plain text)
     * @return User
     */
    public static function select($email, $pass) {
        // Get database connection
        $db = DBConnector::getConnection();

        // Create a new scenario
        try {
            $user = self::verifyUser($email);
            if ($user) {
                $pass_with_salt = $pass . $user->getSalt();

                $pstmt = $db->prepare("SELECT * FROM pss_grr_user WHERE email=:email AND pass=MD5(:pass);");

                // Bind SQL values
                $pstmt->bindValue(':email', $email, PDO::PARAM_STR);
                $pstmt->bindValue(':pass', $pass_with_salt, PDO::PARAM_STR);
                // Execute SQL query
                $pstmt->execute();
                $pstmt = $pstmt->fetch();
                if ($pstmt) {
                    $user = self::create($pstmt);
                    return $user;
                } else {
                    return FALSE;
                }
            }
        } catch (PDOException $e) {
            return FALSE;
        }
        return FALSE;
    }

    
    public static function selectById($id) {
        // Get database connection
        $db = DBConnector::getConnection();
        // Create a new scenario
        try {
                $pstmt = $db->prepare("SELECT * FROM pss_grr_user WHERE idpss_grr_user=:id");
                // Bind SQL values
                $pstmt->bindValue(':id', $id, PDO::PARAM_INT);
                // Execute SQL query
                $pstmt->execute();
                $pstmt = $pstmt->fetch();
                if ($pstmt) {
                    $user = self::create($pstmt);
                    return $user;
                } else {
                    return FALSE;
                }
            
        } catch (PDOException $e) {
            return FALSE;
        }
        return FALSE;
    }
    
    /**
     * Will select a user from the DB. Hellper method 
     * @param type $email
     * @return User
     */
    public static function verifyUser($email) {
        // Get database connection
        $db = DBConnector::getConnection();
        // Create a new scenario
        try {
            // Prepare SQL statement
            $pstmt = $db->prepare("SELECT * FROM pss_grr_user WHERE email=:email;");

            // Bind SQL values
            $pstmt->bindValue(':email', $email, PDO::PARAM_STR);
            // Execute SQL query
            $pstmt->execute();
            $pstmt = $pstmt->fetch();
            if ($pstmt) {
                $user = self::create($pstmt);
                return $user;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            return FALSE;
        }
    }

    public static function insert($email, $pass) {
        $db = DBConnector::getConnection();

        // Create a new scenario
        try {
            if (self::verifyUser($email)) {
                echo $email . 'is in the system. Try to login!';
                return FALSE;
            } else {
                $salt = self::randomSalt();
                $pass_with_salt = $pass . $salt;
                //INSERT INTO pss_grr_user (email, pass, salt) VALUES ('a@a.a', 'a', 'a');
                $pstmt = $db->prepare("INSERT INTO pss_grr_user (email, pass, salt) VALUES (:email, MD5(:pass), :salt);");

                // Bind SQL values
                $pstmt->bindValue(':email', $email, PDO::PARAM_STR);
                $pstmt->bindValue(':pass', $pass_with_salt, PDO::PARAM_STR);
                $pstmt->bindValue(':salt', $salt, PDO::PARAM_STR);
                // Execute SQL query
                if ($pstmt->execute()) {
                    $user = self::verifyUser($email);
                    return $user;
                } else {
                    return FALSE;
                }
            }
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Generates a random string
     * @return String is the salt
     */
    private static function randomSalt() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

}
