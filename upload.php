<?php
require_once('./autoLoader.php');



if(isset($_POST['upload']) && $_FILES["userfile"]['size'] > 0)
{
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["userfile"]["name"]);
        $imageFileType = pathinfo($_FILES["userfile"]['name'],PATHINFO_EXTENSION);

        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
            echo "Sorry, only JPG, JPEG, PNG files are allowed.";
            $uploadOk = 0;
        } 

        if ($_FILES["userfile"]['size'] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        } 
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
            echo "Sorry, only JPG, JPEG, PNG files are allowed.";
            $uploadOk = 0;
        } 

        if ($_FILES["userfile"]['size'] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        } 

        $fileName = $_FILES['userfile']['name'];
        $tmpName  = $_FILES['userfile']['tmp_name'];
        $fileSize = $_FILES['userfile']['size'];
        $fileType = $_FILES['userfile']['type'];
        $check = getimagesize($tmpName); 

        if($check !== false) {
                echo "File is an image - " . $check['mime'] . ".";
                 $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        if($uploadOk==1){
            $image = Image_controller::uploadImage($fileName,2);
            $fileName = ''.$image;
            $target_file = $target_dir . basename($fileName);
            $fp      = fopen($tmpName, 'r');
            $content = fread($fp, filesize($tmpName));
            $content = addslashes($content);
            fclose($fp);

            if(!get_magic_quotes_gpc())
                {
                    $fileName = addslashes($fileName);
                }
            if (move_uploaded_file($_FILES["userfile"]["tmp_name"], $target_file)) {
                echo "The file ". basename( $_FILES["userfile"]["name"]). " has been uploaded.";
                } else {
                echo "Sorry, there was an error uploading your file.";
                }


    }
}

// set a max file size for the html upload form 
$max_file_size = 500000; // size in bytes 

// now echo the html page 
?>

<html lang="en"> 
    <head>      
        <link rel="stylesheet" type="text/css" href="stylesheet.css"> 
         
        <title>Upload form</title> 
     
    </head> 
     
    <body> 
     <form method="post" enctype="multipart/form-data">
        <table width="350" border="0" cellpadding="1" cellspacing="1" class="box">
        <tr>
            <td width="246">
                <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $max_file_size ?>">
                <input name="userfile" type="file" id="userfile">
            </td>
            <td width="80"><input name="upload" type="submit" class="box" id="upload" value=" Upload "></td>
        </tr>
        </table>
      </form> 
    </body> 

</html> 