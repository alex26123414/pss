<!DOCTYPE html>

<?php
require_once('./autoLoader.php');
session_start();

if (isset($_POST['login'])) {
    $email = $_POST['email'];
    $pass = $_POST['pass'];

    $user = User_controller::getUser($email, $pass);
    if ($user) {
        $_SESSION['uid'] = $user->getId();
        $_SESSION['uemail'] = $user->getEmail();
    } else {
        echo 'Invalid email or pass';
    }
}

if (isset($_POST['signup'])) {
    $email = $_POST['email'];
    $pass = $_POST['pass'];

    $user = User_controller::insertUser($email, $pass);
    if ($user) {
        echo '<h1>New account was created! for ' . $user->getEmail() . '</h1>';
        $_SESSION['uid'] = $user->getId();
        $_SESSION['uemail'] = $user->getEmail();
    } else {
        echo '<h1>Somthing went wrong</h1>';
    }
}

if (isset($_POST['logout'])) {
    session_destroy();
    header("Location: index.php");
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Welcome to our amazing PSS</h1>
        <?php if (!isset($_SESSION['uid'])) { ?>
            <form method="POST" action="index.php">
                <input type="email" placeholder="email" name="email" required>
                <input type="password" placeholder="password" name="pass" required>
                <input type="submit" value="Login" name="login">
                <input type="submit" value="Sign Up" name="signup">
            </form>
        <?php } else { ?>
            <h1>Welcome <?php echo $_SESSION['uemail']; ?></h1>
            <h1><a href="display.php">Go to Your Photos</a></h1>
            <form method="POST" action="index.php">
                <input type="submit" value="Log out" name="logout">
            </form>
        <?php } ?>

    </body>
</html>
