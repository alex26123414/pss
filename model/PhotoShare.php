<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PhotoShare
 *
 * @author alex
 */
class PhotoShare {
    //put your code here
    private $id;
    private $user;
    private $photo;
    
    function getId() {
        return $this->id;
    }

    function getUser() {
        return $this->user;
    }

    function getPhoto() {
        return $this->photo;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUser($user) {
        $this->user = $user;
    }

    function setPhoto($photo) {
        $this->photo = $photo;
    }


}
