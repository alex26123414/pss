<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Comment
 *
 * @author alex
 */
class Comment {
    //put your code here
    private $id;
    private $photo;
    private $user;
    private $comment;
    
    function __construct() {
        
    }
    function getId() {
        return $this->id;
    }

    function getPhoto() {
        return $this->photo;
    }

    function getUser() {
        return $this->user;
    }

    function getComment() {
        return $this->comment;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPhoto($photo) {
        $this->photo = $photo;
    }

    function setUser($user) {
        $this->user = $user;
    }

    function setComment($comment) {
        $this->comment = $comment;
    }


}
