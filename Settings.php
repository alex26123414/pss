<?php

class Settings {

    public static function getRoot() {
        return realpath($_SERVER["DOCUMENT_ROOT"]) . DIRECTORY_SEPARATOR . self::getHomeFolder() . DIRECTORY_SEPARATOR;
    }

    public static function getHomeFolder() {
        // Project folder
        return "PSS";
    }

    public static function getServerUrl() {
        return self::getProtocol() . $_SERVER['HTTP_HOST'] . "/" . self::getHomeFolder();
    }

    public static function getProtocol() {
        return "http://";
    }

}
