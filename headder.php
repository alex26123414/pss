<!DOCTYPE html>
<?php
require_once('./autoLoader.php');
session_start();

if (isset($_POST['login'])) {
    $email = $_POST['email'];
    $pass = $_POST['pass'];

    $user = User_controller::getUser($email, $pass);
    if ($user) {
        $_SESSION['uid'] = $user->getId();
        $_SESSION['uemail'] = $user->getEmail();
    } else {
        echo '<div class="alert alert-danger" role="alert">Invalid email or pass</div>';
    }
}

if (isset($_POST['signup'])) {
    $email = $_POST['email'];
    $pass = $_POST['pass'];

    $user = User_controller::insertUser($email, $pass);
    if ($user) {
        echo '<div class="alert alert-success" role="alert">New account was created! for ' . $user->getEmail() . '</div>';
        $_SESSION['uid'] = $user->getId();
        $_SESSION['uemail'] = $user->getEmail();
    } else {
        echo '<div class="alert alert-danger" role="alert">Somthing went wrong!</div>';
    }
}

if (isset($_POST['logout'])) {
    session_destroy();
    header("Location: index_new.php");
}
?>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>PSS</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>

    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index_new.php">Photo Sharing Service</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <?php if (!isset($_SESSION['uid'])) { ?>
                        <form class="navbar-form navbar-right" role="form" method="POST" action="index_new.php">
                            <div class="form-group">
                                <input type="email" placeholder="Email" class="form-control" name="email" required>
                            </div>
                            <div class="form-group">
                                <input type="password" placeholder="Password" class="form-control" name="pass" required>
                            </div>
                            <button type="submit" class="btn btn-success" value="Login" name="login">Login</button>
                            <button type="submit" class="btn btn-info" value="Sign Up" name="signup">Sign in</button>
                        </form>
                    <?php } else { ?>
                        <form class="navbar-form navbar-right" role="form" method="POST" action="index_new.php">
                            <a href="upload_new.php" class="btn btn-primary">Add a new Photo</a>
                            <a href="display_new.php" class="btn btn-primary">Go to Your Photos</a>
                            <button type="submit" class="btn btn-danger" value="Log out" name="logout">Log out <b><?php echo $_SESSION['uemail']; ?></b></button>
                            
                        </form>
                    <?php } ?>
                </div><!--/.navbar-collapse -->
            </div>
        </nav>