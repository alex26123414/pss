-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2015 at 03:54 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pss`
--
CREATE DATABASE IF NOT EXISTS `pss` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pss`;

-- --------------------------------------------------------

--
-- Table structure for table `pss_grr_comment`
--

DROP TABLE IF EXISTS `pss_grr_comment`;
CREATE TABLE IF NOT EXISTS `pss_grr_comment` (
`idpss_grr_comment` int(11) NOT NULL,
  `id_photo` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `comment` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pss_grr_photo`
--

DROP TABLE IF EXISTS `pss_grr_photo`;
CREATE TABLE IF NOT EXISTS `pss_grr_photo` (
`idpss_grr_photo` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pss_grr_photo_share`
--

DROP TABLE IF EXISTS `pss_grr_photo_share`;
CREATE TABLE IF NOT EXISTS `pss_grr_photo_share` (
`idpss_grr_photo_share` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_photo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pss_grr_user`
--

DROP TABLE IF EXISTS `pss_grr_user`;
CREATE TABLE IF NOT EXISTS `pss_grr_user` (
`idpss_grr_user` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(500) NOT NULL,
  `salt` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pss_grr_comment`
--
ALTER TABLE `pss_grr_comment`
 ADD PRIMARY KEY (`idpss_grr_comment`), ADD KEY `fk_c_u_idx` (`id_photo`), ADD KEY `fk_c_u_idx1` (`id_user`);

--
-- Indexes for table `pss_grr_photo`
--
ALTER TABLE `pss_grr_photo`
 ADD PRIMARY KEY (`idpss_grr_photo`), ADD KEY `fk_p_u_idx` (`id_user`);

--
-- Indexes for table `pss_grr_photo_share`
--
ALTER TABLE `pss_grr_photo_share`
 ADD PRIMARY KEY (`idpss_grr_photo_share`), ADD KEY `fk_ph_sh_us_idx` (`id_user`), ADD KEY `fk_ph_sh_ph_idx` (`id_photo`);

--
-- Indexes for table `pss_grr_user`
--
ALTER TABLE `pss_grr_user`
 ADD PRIMARY KEY (`idpss_grr_user`), ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pss_grr_comment`
--
ALTER TABLE `pss_grr_comment`
MODIFY `idpss_grr_comment` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pss_grr_photo`
--
ALTER TABLE `pss_grr_photo`
MODIFY `idpss_grr_photo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pss_grr_photo_share`
--
ALTER TABLE `pss_grr_photo_share`
MODIFY `idpss_grr_photo_share` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pss_grr_user`
--
ALTER TABLE `pss_grr_user`
MODIFY `idpss_grr_user` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `pss_grr_comment`
--
ALTER TABLE `pss_grr_comment`
ADD CONSTRAINT `fk_c_p` FOREIGN KEY (`id_photo`) REFERENCES `pss_grr_photo` (`idpss_grr_photo`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_c_u` FOREIGN KEY (`id_user`) REFERENCES `pss_grr_user` (`idpss_grr_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pss_grr_photo`
--
ALTER TABLE `pss_grr_photo`
ADD CONSTRAINT `fk_p_u` FOREIGN KEY (`id_user`) REFERENCES `pss_grr_user` (`idpss_grr_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pss_grr_photo_share`
--
ALTER TABLE `pss_grr_photo_share`
ADD CONSTRAINT `fk_ph_sh_ph` FOREIGN KEY (`id_photo`) REFERENCES `pss_grr_photo` (`idpss_grr_photo`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ph_sh_us` FOREIGN KEY (`id_user`) REFERENCES `pss_grr_user` (`idpss_grr_user`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
