<?php
/**
 * Description of Security
 *
 * @author Andreas
 */
class Security {
    
    //Method used for hashing passwords
    //Recieves a string and salt
    //If no salt is send as parameter, a random salt will be created
    //To use function without salt, pass -1 or empty string
    //Returns an array with the hash and the salt as hex
    public static function hashValue($pass, $salt = -1){
        
        
        //If salt isn't set or -1 is passed, create a new random salt'
        if($salt == -1){
            
            $salt = mcrypt_create_iv(4, MCRYPT_DEV_URANDOM);
        }
        
        //Add the random salt to the password string
        $pass .= $salt;
        
        //Add a hardcoded salt for extra security
        $pass .= 'a9u2j8y4k15s';
        
        //Hashes the password
        $hashedPass = hash('SHA512', $pass, TRUE);
        
        //Create associative array with hash value and salt value
        $result = array('hash' => $hashedPass, 'salt' => $salt);
        
        
        return $result;
    }
    
    //Method used to sanitize input, preventing SQL-injections etc.
    public static function sanitize($input, $linebreak = FALSE){
        
        //Removes scripting code using preg_replace method
        $input = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', '', $input); 
    
        //Removes all HTML tags using strip_tags method
        $input = strip_tags($input);
        
        //Replace all linebreaks with n12br method:
        if($linebreak){
            $input = n12br($input, TRUE);
        }
        
        //Return the sanitized input
        return $input;
    }
    
    //Used to encrypt text
    public static function encryptMessage($message){
        
        
    }
    
    //Used to decrypt text
    public static function decryptMessage($message){
        
    }
    
    
    //Method used to check for Cross-site request forgery
    public static function csrfCheck(){
        
        $addr = str_split($_SERVER['HTTP_REFERER'], strlen(Settings::getDomain()));
    
        return $addr[0] == Settings::getDomain();
    }
}

?>
