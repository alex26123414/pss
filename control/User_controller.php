<?php
require_once '\Settings.php';

require_once Settings::getRoot().'mapper\User_mapper.php';
require_once Settings::getRoot().'model\User.php';

class User_controller {
    public static function getUser($email, $pass) {
        $scenario = User_mapper::select($email, $pass);
        // Return the results
        return $scenario;
    }
    
    public static function insertUser($email, $pass) {
        $scenario = User_mapper::insert($email, $pass);
        // Return the results
        return $scenario;
    }
    
    public static function getUserById($id) {
        $scenario = User_mapper::selectById($id);
        // Return the results
        return $scenario;
    }
    
}
