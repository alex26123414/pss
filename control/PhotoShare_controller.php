<?php

require_once '\Settings.php';

require_once Settings::getRoot().'mapper\PhotoShare_mapper.php';
require_once Settings::getRoot().'model\PhotoShare.php';

class PhotoShare_controller {
    
    public static function shareImage($email,$photoId) 
       {
        $scenario = PhotoShare_mapper::share($email,$photoId);
        // Return the results
        return $scenario;
       }
}
