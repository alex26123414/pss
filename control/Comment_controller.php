<?php

require_once '\Settings.php';

require_once Settings::getRoot().'mapper\Comment_mapper.php';
require_once Settings::getRoot().'model\Photo.php';

class Comment_controller {
    
    //Select specific comment from given id:
    public static function getComment($id){
        
        $scenario = Comment_mapper::select($id);
                
        return $scenario;
    }
    
    //Select comments for specific photo id:
    public static function getComments($photoId){
        
        $scenario = Comment_mapper::selectComments($photoId);
        
        return $scenario;
    }
    
    public static function inserComment($photoId, $userId, $txt){
        
        $scenario = Comment_mapper::insert($photoId, $userId, $txt);
        
        return $scenario;
    }
    
}