<?php

require_once '\Settings.php';

require_once Settings::getRoot().'mapper\Image_mapper.php';
require_once Settings::getRoot().'model\Photo.php';

class Image_controller {
    //put your code here
       public static function getImage($id) 
       {
        $scenario = Image_mapper::select($id);
        // Return the results
        return $scenario;
       }
       public static function uploadImage($name,$userId) 
       {
        $scenario = Image_mapper::insert($name,$userId);
        // Return the results
        return $scenario;
       }
}
