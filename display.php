<?php
require_once('./autoLoader.php');


if (isset($_POST['share'])) {
    $email = $_POST['email'];
    $id = $_POST['id'];
    PhotoShare_controller::shareImage($email, $id);
}
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Display Images</h1>
        <?php
        $id = $_GET['id'];
        $image = Image_controller::getImage($id);
        if ($image) {
            $maxcols = 3;
            $int = 0;
            echo "<table>";
            echo "<tr>";
            foreach ($image as $i) {
                if ($int == $maxcols) {
                    $int = 0;
                    echo "</tr><tr>";
                }
                ?>
            <td>
                <img src="uploads/<?php echo $i; ?>" style="width:304px; height:228px"/>
                <br>
                <form method="POST" action="display.php">
                    <input type="email" placeholder="Insert Email" name="email">
                    <input type="submit" value="Share User" name="share">
                    <input type ="hidden" name="id" value="<?php echo $i; ?>">
                </form>
            </td>
            <?php
            $int++;
        }
        while ($int <= $maxcols) {
            echo "<td>&nbsp;</td>";
            $int++;
        }
        echo "</tr>";
        echo "</table>";
    } else
        echo "There are no images";
    ?>
</body>