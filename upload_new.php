<?php include './headder.php'; ?>

<?php if (!isset($_SESSION['uid'])) { ?>
    <div class="alert alert-danger" role="alert">You need to be logged in!</div>
<?php } else { ?>

    <?php
    if (isset($_POST['upload']) && $_FILES["userfile"]['size'] > 0) {
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["userfile"]["name"]);
        $imageFileType = pathinfo($_FILES["userfile"]['name'], PATHINFO_EXTENSION);
        $imageFileType = strtolower($imageFileType);

        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
            echo "Sorry, only JPG, JPEG, PNG files are allowed.";
            $uploadOk = 0;
        }

        if ($_FILES["userfile"]['size'] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        
        if ($_FILES["userfile"]['size'] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }

        $fileName = $_FILES['userfile']['name'];
        $tmpName = $_FILES['userfile']['tmp_name'];
        $fileSize = $_FILES['userfile']['size'];
        $fileType = $_FILES['userfile']['type'];
        $check = getimagesize($tmpName);

        if ($check !== false) {
            echo "File is an image - " . $check['mime'] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
        if ($uploadOk == 1) {
            $uid = $_SESSION['uid'];
            $image = Image_controller::uploadImage($fileName, $uid);
            $fileName = '' . $image;
            $target_file = $target_dir . basename($fileName);
            $fp = fopen($tmpName, 'r');
            $content = fread($fp, filesize($tmpName));
            $content = addslashes($content);
            fclose($fp);

            if (!get_magic_quotes_gpc()) {
                $fileName = addslashes($fileName);
            }
            if (move_uploaded_file($_FILES["userfile"]["tmp_name"], $target_file)) {
                echo "The file " . basename($_FILES["userfile"]["name"]) . " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }

// set a max file size for the html upload form 
    $max_file_size = 500000; // size in bytes 
// now echo the html page 
    ?>
    <div class="jumbotron">
        <div class="container">
            <h1><b><?php echo $_SESSION['uemail']; ?> </b> upload a new photo</h1>
        </div>
    </div>
    <div class="container">
        <form method="post" enctype="multipart/form-data">
            
            <input name="userfile" type="file" id="userfile" required><br>
            <button type="submit" class="btn btn-success" name="upload" value=" Upload ">Upload</button>
        </form> 

    <?php } // from else  ?>

    <?php include './footer.php'; ?>