<?php include './headder.php'; ?>

<?php if (!isset($_SESSION['uid'])) { ?>
    <div class="alert alert-danger" role="alert">You need to be logged in!</div>
<?php } else { ?>
    <div class="jumbotron">
        <div class="container">
            <h1><b><?php echo $_SESSION['uemail']; ?>'s</b> Photos</h1>
        </div>
    </div>
    <div class="container">

        <?php
        if (isset($_POST['share'])) {
            $email = $_POST['email'];
            $id = $_POST['id'];
            PhotoShare_controller::shareImage($email, $id);
        }
        if (isset($_POST['comment'])) {
            $img_id = $_POST['id'];
            $user_id = $_SESSION['uid'];
            $text = $_POST['comment_txt'];
            Comment_controller::inserComment($img_id, $user_id, $text);
        }
        ?>

        <?php
        $id = $_SESSION['uid'];
        $image = Image_controller::getImage($id);
        if ($image) {
            $maxcols = 3;
            $int = 0;
            echo "<table>";
            echo "<tr>";
            foreach ($image as $i) {
//                echo '<h1>';
//                var_dump($i);
//                echo '</h1>';
//                $uid_owner = $i->getUser();
//                $user_of_img = User_controller::getUserById($uid_owner);
//                var_dump($user_of_img);
                if ($int == $maxcols) {
                    $int = 0;
                    echo "</tr><tr>";
                }
                ?>
                <td>
                    <img src="uploads/<?php echo $i; ?>" style="width:304px; height:228px"/>
                    <br>
                    <h3>Comments</h3>
                    <?php $comments = Comment_controller::getComments($i); ?>
                    <?php foreach ($comments as $comment) { 
                        $com_user = User_controller::getUserById($comment->getUser());
                        $com_usr_name = $com_user->getEmail();
                        ?>
                        <div>By <b><?php echo $com_usr_name; ?></b>: <?php echo $comment->getComment(); ?></div>
                    <?php } ?>


                    <br>
                    <form method="POST" action="display_new.php">
                        <input type="email" placeholder="Insert Email" name="email" required>
                        <button type="submit" class="btn btn-success" value="Share User" name="share">Share</button>
                        <input type ="hidden" name="id" value="<?php echo $i; ?>">
                    </form>
                    <br>
                    <form method="POST" action="display_new.php">
                        <input type="text" placeholder="Insert Comment "name="comment_txt" required>
                        <button type="submit" class="btn btn-success" name="comment" value="Comment">Comment</button>
                        <input type ="hidden" name="id" value="<?php echo $i; ?>">
                    </form>
                </td>
                <?php
                $int++;
            }
            while ($int <= $maxcols) {
                echo "<td>&nbsp;</td>";
                $int++;
            }
            echo "</tr>";
            echo "</table>";
        } else
            echo "There are no images";
        ?>
    <?php } // from else  ?>

    <?php include './footer.php'; ?>